#!/bin/bash



# Only the F=0.4 cases 
nevents=100
F=0.4
for seed_prefix in {10..19}
do
    rseed=${seed_prefix}${N}1234567

    N=10
    time=1200m
    label=${N}_${F}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed

    N=25
    time=3000m
    label=${N}_${F}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed

    N=50
    time=6000m
    label=${N}_${F}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed

    N=100
    time=10000m
    label=${N}_${F}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed

    N=200
    time=10000m
    label=${N}_${F}_${rseed}
    sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed

done


# # 100 events times 10 seeds = 1000 events in 10 files per N per F
# # 5 each of N and F values
# # So 250 files, which should be easy to deal with
# # http://serverfault.com/questions/98235/how-many-files-in-a-directory-is-too-many-downloading-data-from-net
# nevents=100


# N=10
# time=1200m
# for F in 0.4 0.5 0.75 1.0 1.5
# do
#     for seed_prefix in {10..19}
#     do
# 	rseed=${seed_prefix}${N}1234567
# 	label=${N}_${F}_${rseed}
# 	sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed
#     done
# done


# N=25
# time=600m
# for F in 0.4 0.5 0.75 1.0 1.5
# do
#     for seed_prefix in {10..19}
#     do
# 	rseed=${seed_prefix}${N}1234567
# 	label=${N}_${F}_${rseed}
# 	sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed
#     done
# done


# N=50
# time=1200m
# for F in 0.4 0.5 0.75 1.0 1.5
# do
#     for seed_prefix in {10..19}
#     do
# 	rseed=${seed_prefix}${N}1234567
# 	label=${N}_${F}_${rseed}
# 	sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed
#     done
# done


# N=100
# time=2000m
# for F in 0.4 0.5 0.75 1.0 1.5
# do
#     for seed_prefix in {10..19}
#     do
# 	rseed=${seed_prefix}${N}1234567
# 	label=${N}_${F}_${rseed}
# 	sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed
#     done
# done


# N=200
# time=4000m
# for F in 0.4 0.5 0.75 1.0 1.5
# do
#     for seed_prefix in {10..19}
#     do
# 	rseed=${seed_prefix}${N}1234567
# 	label=${N}_${F}_${rseed}
# 	sqsub -q serial -o logs/log_${label}.log -r $time ~/espresso-2.1.2j/Espresso nanopore.tcl $N $F 0 $nevents $rseed
#     done
# done



