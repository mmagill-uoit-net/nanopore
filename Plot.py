import numpy as np
import matplotlib.pyplot as plt
import matplotlib

# Load the data
#thenpz = np.load('ScalingLaw_rnom15_wbug.npz')
thenpz = np.load('ScalingLaw.npz')
Ns = thenpz['Ns']
Fs = thenpz['Fs']
avgtaus = thenpz['avgtaus']

# Remove N=10 point
Ns = Ns[1:]
avgtaus = avgtaus[1:,:]

# Taus are in units of # of timesteps, so divide by 100 to get sim time
avgtaus = avgtaus/100.


### Only one F
F = 0.4

# Plot
plt.loglog(Ns,avgtaus[:,Fs==F],'*',markersize=13)

# Fit the data
FitLine = np.polyfit(np.log(Ns[0:]),np.log(avgtaus[0:,Fs==F]),1)
xmin = 5
xmax = 250
fineNs = np.linspace(xmin,xmax,100)
plt.loglog(fineNs,np.exp(FitLine[1])*fineNs**FitLine[0],'k--')
plt.text(5*xmin,1000,r'$\tau \,\sim\, N^{\,%.2f}$' % FitLine[0])

# Settings
plt.xlim(xmin,xmax)
plt.ylim(20,10000)
plt.xlabel('Chain Length, N')
plt.ylabel(r"Translocation Time, $\tau$")
matplotlib.rcParams.update({'font.size': 22})
plt.subplots_adjust(left=0.14, bottom=0.15, right=0.98, top=0.94, wspace=0.2, hspace=0.2)
plt.show()






