import numpy as np
import matplotlib.pyplot as plt
import glob


# All ntrans files
list = glob.glob('data/*tau*')


# Big array to store data
# Each row is:
##  N,tau
Nevents=100
data = np.ndarray((len(list)*Nevents,3),dtype=float)


# Count number of lines in each, which is tau
i = 0
for file in list:

    # Get chain length
    N = int(file.split('/')[1].split('_')[0])

    # Get the field strength
    F = float(file.split('/')[1].split('_')[1])

    # Get translocation times
    with open(file,'r') as fin:
        for line in fin:
            tau = int(line)
            data[i,0] = N
            data[i,1] = F
            data[i,2] = tau
            i = i + 1


# Plot average taus for each N
Ns = np.unique(data[:,0])
Fs = np.unique(data[:,1])
avgtaus = np.ndarray((Ns.size,Fs.size),dtype=float)
i = 0
for N in Ns:
    j = 0
    for F in Fs:
        taus = data[(data[:,0]==N)&(data[:,1]==F),2]
        avgtaus[i,j] = np.mean(taus)
        j = j+1
    i = i+1


np.savez('ScalingLaw.npz',Ns=Ns,Fs=Fs,avgtaus=avgtaus)
