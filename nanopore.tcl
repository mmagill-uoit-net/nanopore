# Example input:
# ~/espresso-2.1.2j/Espresso nanopore.tcl 20 1. 0 10 21341435

# Parse input
set N            [lindex $argv 0]
set F            [lindex $argv 1]
set vis_flag     [lindex $argv 2]
set nevents      [lindex $argv 3]
set rseed        [lindex $argv 4]
t_random seed    $rseed

########################################################################################

# Function runs a single translocation
global VMD_IS_ON 
set VMD_IS_ON 0
proc RunMain { N F vis_flag rseed casenum} {
    global VMD_IS_ON 

    # Output files
    #set CaseName "${N}_${F}_${rseed}_${casenum}"
    set CaseName "${N}_${F}_${rseed}"
    #set Ftrans   [open "data/${CaseName}_ntrans.dat"   "w"]
    set Ffail  [open "data/${CaseName}_fail.dat"  "a+"]
    set Ftau  [open "data/${CaseName}_tau.dat"  "a+"]

    # Scales
    set sig 1.0
    set eps 1.0

    # Domain size
    set box_l 400

    # Thermostat Parameters
    set temp 1.
    set gamma 1.

    # FENE Potential Parameters
    set kap [expr {30.0*$eps/($sig*$sig)}]
    set lam [expr {1.5*$sig}]

    # LJ Potential Parameters
    set cut   [expr {pow(2.0,1.0/6.0)*$sig}]
    set shift [expr {0.25*$eps}]


    # Geometry Parameters
    set rnom         1.5
    set tnom         0.0001
    set teff [expr {$tnom + $sig}]

    ##########################################################################


    # Spatial domain creation
    setmd box_l $box_l $box_l $box_l

    # Temporal domain creation
    setmd time_step 0.01
    setmd skin 0.4

    # Interaction creations
    inter 0 fene $kap $lam
    inter 7 angle 0
    inter 0 0  lennard-jones $eps $sig $cut $shift 0.
    inter 0 76 lennard-jones $eps $sig $cut $shift 0.

    # Pore creation
    constraint pore center [expr {$box_l/2.}] [expr {$box_l/2.}] [expr {$box_l/2.}]  axis 0 0 1  radius $rnom  length $tnom  type 76

    # Fake pore monomers
    if { $vis_flag == 1 } {
	# Number of fake monomer branches
	set N_noms 2
	for { set i 0 } { $i < $N_noms } { incr i } {
	    part [expr {$N+$i+0*$N_noms}] pos [expr {$box_l/2. + ($rnom + $i*$sig)}] [expr {$box_l/2.}] [expr {$box_l/2.}] type 1 fix 1 1 1 
	    part [expr {$N+$i+1*$N_noms}] pos [expr {$box_l/2. - ($rnom + $i*$sig)}] [expr {$box_l/2.}] [expr {$box_l/2.}] type 1 fix 1 1 1 
	}
    }

    # Polymer creation (ncavi at time 0 is threads)
    # Need to offset from absolute center because potential is ill-defined (Espresso Bug)
    set x [expr {$box_l/2. + 0.01}]
    set y [expr {$box_l/2. + 0.01}]
    set z [expr {$box_l/2. + 0.01}]
    part 0 pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.
    # Build the chain (without angular potential)
    for { set i 1 } { $i < $N } { incr i } {
	# Z position
	set z [expr {$z - $sig}]

	# Place the ith particle
	part $i pos $x $y $z type 0 fix 0 0 0 ext_force 0. 0. 0.

	# FENE bond to the previous particle in the chain
	part $i bond 0 [expr {$i - 1}] 
    }
    # After all particles are placed, apply the angular potential (if any)
    for { set i 1 } { $i < [expr {$N-1}] } { incr i } {
	part $i bond 7 [expr {$i - 1}] [expr {$i + 1}]
    }


    ##########################################################################


    # Initialize Visualization
    if { $vis_flag == 1 } {
	if { $VMD_IS_ON == 0 } {
	    prepare_vmd_connection vmdout
	    imd listen 100
	    set VMD_IS_ON 1
	}
	imd positions
    }

    # Equilibrate (fix all threaded monomers)
    puts "Equilibrating..."
    part 0 fix
    thermostat langevin $temp 0.1
    for {set i 1} {$i < 1e3} {incr i 1} {
	integrate 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }
    puts "Equilibrated"
    part 0 unfix

    # Simulate
    thermostat langevin $temp $gamma
    set ntrans 0
    set timestep 0
    while {$ntrans < $N} {
	# Counters
	set ntrans 0

	# Apply E-field
	for { set i 0 } { $i < $N } { incr i } {
	    # Current particle's z coordinate
	    set z [lindex [part $i print pos] 2]

	    # Apply force inside the pore
	    if { $z <= [expr {($box_l/2.) + ($teff/2.)}] && $z >= [expr {($box_l/2.) - ($teff/2.)}] } { 
		part $i ext_force 0 0 $F
	    } else {
		part $i ext_force 0 0 0
	    }

	}

	# Count Positions
	for { set i 0 } { $i < $N } { incr i } {
	    # Current particle's z coordinate
	    set z [lindex [part $i print pos] 2]

	    if {$z >= [expr {$box_l/2.}] } { 
		incr ntrans
	    }
	}

	# If event failed, exit function with error message
	if { $ntrans == 0 } {
	    puts  "Event failed for N of $N."
	    puts  $Ffail "$timestep"
	    #close $Ftrans
	    close $Ffail
	    close $Ftau
	    return 1
	}

	# Integrate (ideally one step at a time, but compromised to 100 for speed)
	#puts $Ftrans "$ntrans"
	integrate 100
	incr timestep 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }

    # Record translocation time
    puts $Ftau "$timestep"

    #close $Ftrans
    close $Ffail
    close $Ftau
    return 0
}


##########################################################################
##########################################################################

# Actually run the program
# Run nevents translocations
for { set casenum 1 } { $casenum <= $nevents } { incr casenum } {

    # Run case
    set errcode 1
    while { $errcode == 1 } {
	set errcode [RunMain $N $F $vis_flag $rseed $casenum]

	# Unbond the polymer (so the function can be called again)
	for { set i 1 } { $i < $N } { incr i } {
	    part $i bond delete
	}   

	# Delete the constraint
	constraint delete
    }
}


